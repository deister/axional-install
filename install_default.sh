#!/bin/bash

# ==============================================================================
# DEISTER, S.A. (Copyright 2016)
# ==============================================================================

# ----- DEFAULT VARIABLES ------------------------------------------
NEXUS_BASE_URL="http://nexus3.deistercloud.com/"
NEXUSUSER=""
NEXUSPASS=""

INSTALLRC_FILE=.`basename "$0" | cut -d'.' -f1`.rc
SRC_FOLDER=AXBIN
TMP_FOLDER=/tmp/deisterAX_tmp.$$
ARR_PRODUCTS=("studio" "microservices" "docs")

# Debug flags as "" (disabled) or "1" (enabled)
DEBUG="1"
DEBUG_CURL=""

# ----- SYSTEM VARIABLES -------------------------------------------

# Get os type , note that it is LOWER-CASED.  Used here and later on
osName=`uname -s 2> /dev/null | $TR "[:upper:]" "[:lower:]" 2> /dev/null`

# Get AWK command
AWK=nawk
( $AWK '{}' ) < /dev/null 2>&0 || AWK=awk

# Get HG (mercurial) command, if exists
: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

HG=""
if command -v hg >/dev/null 2>&1; then
    HG="hg"
fi
'

PATHEXECUTE="$(pwd -P)"
PATHSHELL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PATH=$PATH:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin


check_Prerequisites()
{
    rm -rf $TMP_FOLDER
	if [ ! -d $TMP_FOLDER ]; then
		mkdir -p $TMP_FOLDER
	fi

	[ $DEBUG ] && echo ""

	[ $DEBUG ] && echo -n "* Looking for curl... "
	if [ -z $(which curl) ]; then
		echo "Not found."
		echo ""
		echo "======================================================================================================"
		echo " Please install curl on your system using your favourite package manager."
		echo " Restart after installing curl."
		echo "======================================================================================================"
		echo ""
		exit 0
    else
        echo "OK"
	fi

	[ $DEBUG ] && echo -n "* Looking for unzip... "
	if [ -z $(which unzip) ]; then
		echo "Not found."
		echo ""
		echo "======================================================================================================"
		echo " Please install unzip on your system using your favourite package manager."
		echo " Restart after installing unzip."
		echo "======================================================================================================"
		echo ""
		exit 0
	else
        echo "OK"
	fi

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

	[ $DEBUG ] && echo -n "* Looking for hg (Mercurial)... "
	if ! command -v hg >/dev/null 2>&1; then
		echo "Not found. Not versioning library files..."
		echo ""
	else
        echo "OK"
	fi
'

	[ $DEBUG ] && echo ""
}

showConfigParams()
{
	echo ""
	echo "CONFIGURATION PARAMETERS"
	echo "--------------------------------------"
	echo "NEXUS_BASE_URL: $NEXUS_BASE_URL"
	echo "NEXUSUSER.....: $NEXUSUSER"
	echo "NEXUSPASS.....: **********"
	echo "NEXUSPRODUCT..: $NEXUSPRODUCT"
	echo "PRODUCTNAME...: $PRODUCTNAME"
	echo "PRODUCTVERS...: $PRODUCTVERS"
	echo ""
}

function doCurl
{
	ACTION=$1
	if [ -z "${ACTION}" ]
	then
	    usage "no action supplied"
	fi

	STORE_DIRECTORY=$2
	if [ -z "${STORE_DIRECTORY}" ]
	then
	    error "no libs folder supplied"
	fi

	ASSET_TYPE=$3
	if [ -z "${ASSET_TYPE}" ] || ( [ "${ASSET_TYPE}" != "jar" ] && [ "${ASSET_TYPE}" != "zip" ] && [ "${ASSET_TYPE}" != "war" ] )
	then
	    error "invalid asset type supplied"
	fi

	ASSET_CLASSIFIER=$4
	if [ -z "${ASSET_CLASSIFIER}" ] || ( [ "${ASSET_CLASSIFIER}" != "none" ] && [ "${ASSET_CLASSIFIER}" != "empty" ] )
	then
	    error "invalid asset classifier supplied"
	fi

	DOWNLOAD_PRODUCTNAME=$5
	if [ -z "${DOWNLOAD_PRODUCTNAME}" ]
	then
	    error "invalid product name supplied"
	fi

	NEXUSVERSION=${6:-+}

#TODO: consider adding the following arguments to avoid SSL certificate problems
#TODO: consider adding the following arguments to avoid SSL certificate problems
#TODO: consider adding the following arguments to avoid SSL certificate problems
#TODO: consider adding the following arguments to avoid SSL certificate problems
#TODO: consider adding the following arguments to avoid SSL certificate problems
#TODO: consider adding the following arguments to avoid SSL certificate problems
	#-Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

    if [ "${ACTION}" == "download" ] && [ -n "${HG}" ]; then
        # check if libs folder exists
        if [ -d "${STORE_DIRECTORY}" ]; then
            # check if there are pending changes
            if ! hg summary | grep -q "commit: (clean)" >/dev/null 2>&1; then
                echo "Commiting changes to HG repository before downloading libraries"
                hg addremove -q ${STORE_DIRECTORY}
                if [ $? -ne 0 ]; then
                    echo "An error occurred adding pending files to HG repository. Abort."
                    exit 1
                fi
                hg commit -q -m "Before Gradle action [${ACTION}], version [${NEXUSVERSION}] on folder [${STORE_DIRECTORY}], date `date "+%Y-%m-%d"`" -u `whoami`
                if [ $? -ne 0 ]; then
                    echo "An error occurred while commiting changes to HG repository. Abort."
                    exit 1
                fi
            fi
        fi
    fi
'

#echo "............. global product name is $PRODUCTNAME"
#echo "........ method call product name is $DOWNLOAD_PRODUCTNAME"

#    PRODUCTNAME2=$PRODUCTNAME
#    PRODUCTNAME2=$DOWNLOAD_PRODUCTNAME

#    if [ "${ASSET_TYPE}" == "zip" ]; then
#        PRODUCTNAME2="${PRODUCTNAME2}.dist"
#    fi

	ASSET_TYPE=$3
	if [ "${ACTION}" == "checknexus" ] || [ "${ACTION}" == "download" ]
	then

		CURLCRED="${NEXUSUSER}:${NEXUSPASS}"

		REPOTYPE="snapshots"
# Regexp not working !!!!
# Regexp not working !!!!
# Regexp not working !!!!
# Regexp not working !!!!
# Regexp not working !!!!
#		TAGREGEXP="20[0-9]{2}\.[0-9]{1}.[0-9]+"
#		if [[ "${NEXUSVERSION}" =~ ${TAGREGEXP} ]]; then
#			REPOTYPE="releases"
#		fi
		if [[ "${NEXUSVERSION}" != 0.0.* ]]; then
			REPOTYPE="releases"
		fi

# Use always the same URL for download, no matter if release or snapshot
#        if [ "${NEXUSVERSION}" == "0.0.+" ]; then
#            CURLLOC="${NEXUS_SEARCH_URL}?repository=${REPOTYPE}&name=${DOWNLOAD_PRODUCTNAME}&sort=version&direction=desc&maven.extension=${ASSET_TYPE}"
#        else
#            CURLLOC="${NEXUS_SEARCH_URL}?repository=${REPOTYPE}&name=${DOWNLOAD_PRODUCTNAME}&version=${NEXUSVERSION}&maven.extension=${ASSET_TYPE}"
#
#        fi
		CURLLOC="${NEXUS_SEARCH_URL}?repository=${REPOTYPE}&name=${DOWNLOAD_PRODUCTNAME}&sort=version&direction=desc&maven.extension=${ASSET_TYPE}"

        if [ "${ASSET_CLASSIFIER}" == "empty" ]; then
            CURLLOC="${CURLLOC}&maven.classifier="
        fi

	fi

	echo ""
    echo ""

	if [ "${ACTION}" == "checknexus" ]; then

		[ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        [ $DEBUG_CURL ] && echo "curl -L -s -u '${CURLCRED}' -X GET '${CURLLOC}'"
        [ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        echo "Checking Nexus connection..."

        NEXUS_STATUS=$( curl -u ${CURLCRED} -I ${CURLLOC} 2>/dev/null | head -n 1 | cut -d$' ' -f2 )

		# If Response is not ok or a redirect exit from installer
		if [ "${NEXUS_STATUS}" != "200" ] && [ "${NEXUS_STATUS}" != "301" ] && [ "${NEXUS_STATUS}" != "302" ] && [ "${NEXUS_STATUS}" != "303" ]; then
			echo "Nexus HTTP response is not OK (was ${NEXUS_STATUS}). Exiting..."
			exit 1
		fi

    elif [ "${ACTION}" == "download" ]; then

        [ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        [ $DEBUG_CURL ] && echo "curl -w "%{redirect_url}" -u ${CURLCRED} -X GET ${CURLLOC}"
        [ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        echo "Searching ... ${CURLLOC}"
        DOWNLOADURL="$( curl -w "%{redirect_url}" -u ${CURLCRED} -X GET ${CURLLOC} 2>/dev/null )"

        if ! [[ "$DOWNLOADURL" =~ ^http:\/\/.*|^https:\/\/ ]]; then
            echo "Invalid redirect URL returned [$DOWNLOADURL]. Abort."
            exit 1
        fi

        mkdir -p ${STORE_DIRECTORY}
        rm -f ${STORE_DIRECTORY}/`basename ${DOWNLOADURL}`

        [ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        [ $DEBUG_CURL ] && echo "curl -L -s -u '${CURLCRED}' -X GET '${CURLLOC}'"
        [ $DEBUG_CURL ] && echo "-----------------------------------------------------------------------------------------------------------------"
        echo "Downloading ... ${DOWNLOADURL}"
        [ $DEBUG_CURL ] && echo " ... as ${STORE_DIRECTORY}/`basename ${DOWNLOADURL}`"
        curl -L -s -u ${CURLCRED} -X GET ${CURLLOC} > ${STORE_DIRECTORY}/`basename ${DOWNLOADURL}`

    else
        echo "Action '${ACTION}' not supported. Abort."
        exit 1
    fi

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

    if [ "${ACTION}" == "download" ] && [ -n "${HG}" ]; then
        echo "Commiting changes to HG repository after downloading libraries"
        hg addremove -q ${STORE_DIRECTORY}
        if [ $? -ne 0 ]; then
            echo "An error occurred adding pending files to HG repository. Abort."
            exit 1
        fi
        hg commit -q -m "After Gradle action [${ACTION}], version [${NEXUSVERSION}] on folder [${STORE_DIRECTORY}], date `date "+%Y-%m-%d"`" -u `whoami`
        if [ $? -ne 0 ]; then
            echo "An error occurred while commiting changes to HG repository. Abort."
            exit 1
        fi
    fi
'

}

function getCurrentVersion
{
	if [ -d "libs" ]; then
		FULLVER=`ls libs | grep "${PRODUCTNAME}-" | cut -d'-' -f2`

	    if [ -n "${FULLVER}" ]; then
    		FULLVER=`basename $FULLVER .jar`
            case $1 in
                major)
                    echo $FULLVER | cut -d'.' -f1,2
                    ;;

                minor)
                    echo $FULLVER | cut -d'.' -f3
                    ;;

                *)
                    echo $FULLVER
                    ;;
            esac
	    fi
	fi
}

function getRemoteVersion
{
    usage "NOT YET IMPLEMENTED!"

#	case $1 in
#	    update)
#			VERSIONS=`getRemoteVersionInfo latestSnapshot | cut -d'-' -f1`
#			;;
#
#	    upgrade)
#			VERSIONS=`getRemoteVersionInfo latestRelease | cut -d'-' -f1`
#	       	;;
#
#	    *)
#		    echo "getRemoteVersion: Invalid parameter $1..."
#		    ;;
#	esac
#	echo $VERSIONS
}

function getRemoteVersionInfo
{
    usage "NOT YET IMPLEMENTED!"

#	TAG=$1
#	if [ -z "${TAG}" ]
#	then
#	    usage "no query type (version|latestSnapshot|latestRelease) supplied"
#	fi
#	GROUP=deister
#	curl --insecure -s -u "${NEXUSUSER}:${NEXUSPASS}" -L "${NEXUS_LUCENE_URL}?r=${NEXUS_LUCENE_TYPE}&g=${GROUP}&a=${PRODUCTNAME}" | grep "<${TAG}>" | uniq | sed -e 's/<[^>]*>//g'
}


function getAllRemoteVersionsInfo
{
    usage "NOT YET IMPLEMENTED!"

#	GROUP=deister
#	curl --insecure -s -u "${NEXUSUSER}:${NEXUSPASS}" -L "${NEXUS_LUCENE_URL}?r=${NEXUS_LUCENE_TYPE}&g=${GROUP}&a=${PRODUCTNAME}"
}


# ----------------------------------------------------------------
# Utility functions
# ----------------------------------------------------------------
isValidNumber()
{
    $AWK 'BEGIN {
            if ( ARGV[1] ~ /^[0-9]+$/ ) {
                    exit(0);
            } else {
                    exit(1);
            }
         }' "$1"

    return $?
}

arrayContainsElement ()
{
  local e
  for e in "${@:2}"; do
  	if [[ "$e" == "${1}" ]]; then
  		echo y
  		return 0
  	fi
  done
  echo n
  return 1
}

inputConfirmation ()
{
	local MSG=`[[ -z "${1}" ]] && echo "Are You Sure? [y/n]" || echo "${1}"`

	while [[ true ]]; do
		read -r -p "${MSG} " input

		case $input in
		    [yY][eE][sS]|[yY]|[sS])
				echo "Yes"
				exit 0
				;;

		    [nN][oO]|[nN])
				echo "No"
				exit 0
		       	;;

		    *)
			echo "Invalid input..."
			;;
		esac
	done
}

command_Info()
{
	echo ""
	echo -n "Current version: "
	echo `getCurrentVersion`

	echo -n "Update version : "
	getRemoteVersion update

	echo -n "Upgrade version: "
	getRemoteVersion upgrade
	echo ""
}

command_Setup()
{
	# Called at the end of script
	echo ""
	echo "Setup parameters saved."
	echo ""
}

command_SelfUpdate()
{
    if [[ `inputConfirmation "Do you want to update this installer? [y/n] "` == "Yes" ]]; then
        if [[ `inputConfirmation "Sure? Any modifications made to the current script or the other files will be lost... [y/n] "` == "Yes" ]]; then
        	curl -k -s "https://bitbucket.org/deister/axional-install/get/master.tar.gz" | tar -x -z --strip-components=1 --exclude ".git*"
        fi
    fi
}

command_Download()
{
    CUST_LIBS_FOLDER="$1"
	if [ -z "${CUST_LIBS_FOLDER}" ]; then
        read -r -p "Path for product libraries: " INP
        [ -n "$INP" ] && CUST_LIBS_FOLDER="$INP"
	fi

    if [ -z "${CUST_LIBS_FOLDER}" ]; then
        echo ""
	    echo "Library folder not valid..."
	    echo ""
		exit 1
	fi

    if [ -f ${CUST_LIBS_FOLDER} ]; then
        echo ""
		echo "Library folder already exists..."
		echo ""
		exit 1
	fi

    INS_PRODUCTNAME="$2"
    if [ -z "$INS_PRODUCTNAME" ]; then
		echo ""
		echo "ERROR: product name not supplied"
		echo ""
		exit 1
	fi

	# Check Nexus credentials
	# Values for asset_type / asset_classifier will not be used for checking!!!
	doCurl "checknexus" "${CUST_LIBS_FOLDER}" "jar" "none" ${INS_PRODUCTNAME} ${PRODUCTVERS}

    if [ "${INS_PRODUCTNAME}" == "axional.studio.startup" ]; then
    	#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${CUST_LIBS_FOLDER}" "jar" "none"  ${INS_PRODUCTNAME} ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "zip" "none"  "${INS_PRODUCTNAME}.dist" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "war" "none"  "axional.studio.webapps" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "war" "none"  "axional.studio.web.view.webapp" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "war" "none"  "axional.studio.web.os.webapp" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "jar" "empty" "axional.server.boot" "0.0.+" #${PRODUCTVERS}
    elif [ "${INS_PRODUCTNAME}" == "axional.server.microservices" ]; then
    	#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${CUST_LIBS_FOLDER}" "jar" "none"  "`basename ${INS_PRODUCTNAME} -fatjar`" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "zip" "none"  "`basename ${INS_PRODUCTNAME} -fatjar`.dist" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "jar" "empty" "axional.server.boot" "0.0.+" #${PRODUCTVERS}
	elif [ "${INS_PRODUCTNAME}" == "axional.docs" ]; then
		#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${CUST_LIBS_FOLDER}" "jar" "none"  "`basename ${INS_PRODUCTNAME} -fatjar`" ${PRODUCTVERS}
        doCurl "download" "${CUST_LIBS_FOLDER}" "zip" "none"  "`basename ${INS_PRODUCTNAME} -fatjar`.dist" ${PRODUCTVERS}
    else
        echo ""
        echo "Product name not supported: ${INS_PRODUCTNAME}"
        echo ""
        exit 1
    fi
}

command_Install()
{
    INS_PRODUCTNAME=$1
    if [ -z "$INS_PRODUCTNAME" ]; then
		echo ""
		echo "ERROR: product name not supplied"
		echo ""
		exit 1
	fi

	CURRVER=`getCurrentVersion`
	if [ -n "$CURRVER" ]; then
		echo ""
		echo "ERROR: Version $CURRVER already installed. Update or upgrade product"
		echo ""
		exit 1
	fi

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

	if [ -n "${HG}" ]; then
        if [ ! -f "$PATHEXECUTE/.hgignore" ]; then
            cat <<'EOF' > "$PATHEXECUTE/.hgignore"
syntax: glob
*.log
*.log.last
syntax: regexp
^tmp$
^logs$
EOF
        fi

        echo "* Initializing HG repository"
        hg init
        if [ $? -ne 0 ]; then
            echo "ERROR: An error occurred while initializing HG repository. Abort."
            exit 1
        fi
    fi
'

	# Check Nexus credentials
	# Values for asset_type / asset_classifier will not be used for checking!!!
	doCurl "checknexus" "${LIBS_FOLDER}" "jar" "none" ${INS_PRODUCTNAME} ${PRODUCTVERS}

    if [ "${INS_PRODUCTNAME}" == "axional.studio.startup" ]; then
    	#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${LIBS_FOLDER}" "jar" "none"  ${INS_PRODUCTNAME} ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "zip" "none"  "${INS_PRODUCTNAME}.dist" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "war" "none"  "axional.studio.webapps" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "war" "none"  "axional.studio.web.view.webapp" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "war" "none"  "axional.studio.web.os.webapp" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "jar" "empty" "axional.server.boot" "0.0.+" #${PRODUCTVERS}
    elif [ "${INS_PRODUCTNAME}" == "axional.server.microservices" ]; then
    	#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${LIBS_FOLDER}" "jar" "none" "`basename ${INS_PRODUCTNAME} -fatjar`" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "zip" "none" "`basename ${INS_PRODUCTNAME} -fatjar`.dist" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "jar" "empty" "axional.server.boot" "0.0.+" #${PRODUCTVERS}
	elif [ "${INS_PRODUCTNAME}" == "axional.docs" ]; then
		#      action     target_folder         asset_type asset_classifier product_name
        doCurl "download" "${LIBS_FOLDER}" "jar" "none" "`basename ${INS_PRODUCTNAME} -fatjar`" ${PRODUCTVERS}
        doCurl "download" "${LIBS_FOLDER}" "zip" "none" "`basename ${INS_PRODUCTNAME} -fatjar`.dist" ${PRODUCTVERS}
    else
        echo ""
        echo "Product name not supported: ${INS_PRODUCTNAME}"
        echo ""
        exit 1
    fi

	DISTFILE=`ls libs/${INS_PRODUCTNAME}*.zip 2>/dev/null`
	if [[ -n "$DISTFILE" ]] && [[ -f "$DISTFILE" ]]; then
        TMP_ROOT=tmp-${INS_PRODUCTNAME}-$$
		unzip -q -o -d $TMP_ROOT $DISTFILE
		for F in `ls -d $TMP_ROOT/*/*`; do
			if [ -d `basename $F` ]; then
				rm -rf `basename $F`
			fi
			mv -f $F .
		done
		rm -rf $TMP_ROOT

		if [[ -f bin/setup.sh ]]; then
			chmod a+x bin/setup.sh
			bin/setup.sh
		fi
	fi

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

    if [ "${ACTION}" == "download" ] && [ -n "${HG}" ]; then
        echo "Commiting changes to HG repository after product setup"
        hg addremove -q
        if [ $? -ne 0 ]; then
            echo "ERROR: An error occurred adding pending files to HG repository. Abort."
            exit 1
        fi
        hg commit -q -m "After install command, date `date "+%Y-%m-%d"`" -u `whoami`
        if [ $? -ne 0 ]; then
            echo "ERROR: An error occurred while commiting changes to HG repository. Abort."
            exit 1
        fi
    fi
'

#		doBackup "bin conf dll scripts"
#	    extractDistFiles
#		setupProductConfig
#		setupStudioWAR

}

command_Update()
{
	declare -a folders=( "bin" "conf" "docs-js" "scripts" "jdbc" )

	for fname in "${folders[@]}"
	do
		echo "Moving ${fname} to ${fname}.$$..."
		mv "${fname}" "${fname}.$$"
	done

	rm -rf libs_old
	mv libs libs_old

	command_Install $1

	for fname in "${folders[@]}"
	do
		echo "Restoring contents "
		cp -R ${fname}.$$/* ${fname}/
	done
}

command_Update_old()
{
    echo ""
    echo "ERROR: update option not yet implemented"
    echo ""
    exit 1

    CURRVER=`getCurrentVersion`
    UPDTVER=`getRemoteVersion update`
	if [ "$CURRVER" == "$UPDTVER" ]; then
			echo ""
			echo ""
			echo "ERROR: Last available version $CURRVER already installed."
		exit 1
	fi

#	doCurl "download" "${LIBS_FOLDER}" "jar" ${PRODUCTVERS}
	doCurl "download" "${LIBS_FOLDER}" "zip" ${PRODUCTVERS}

	DISTFILE=`ls libs/${PRODUCTNAME}*.zip 2>/dev/null`
	if [[ -n "$DISTFILE" ]] && [[ -f "$DISTFILE" ]]; then
		ROOT_FOLDER=`basename ${DISTFILE##*/} .zip`
		if [ -d $TMP_FOLDER/$ROOT_FOLDER ]; then
			rm -rf $TMP_FOLDER/$ROOT_FOLDER
		fi
		unzip -q -d $TMP_FOLDER $DISTFILE
		NEW_DIST_FOLDER="_ver_$UPDTVER"
		if [ -d $NEW_DIST_FOLDER ]; then
			rm -rf $NEW_DIST_FOLDER
		fi
		mkdir -p $NEW_DIST_FOLDER
		for F in `ls -d $TMP_FOLDER/$ROOT_FOLDER/*`; do
#			if [ -d `basename $F` ]; then
#				rm -rf `basename $F`
#			fi
			mv -f $F $NEW_DIST_FOLDER/
		done
		rm -rf $TMP_FOLDER/$ROOT_FOLDER

		# setup.sh Should be smart and detect if executing from an update or first install
		if [[ -f bin/setup.sh ]]; then
			chmod a+x bin/setup.sh
			bin/setup.sh
		fi
	fi

: ' TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.
    TODO: migrate versioning feature to GIT. Now dissabled.

    if [ "${ACTION}" == "download" ] && [ -n "${HG}" ]; then
        echo "Commiting changes to HG repository after product update"
        hg addremove -q
        if [ $? -ne 0 ]; then
            echo "ERROR: An error occurred adding pending files to HG repository. Abort."
            exit 1
        fi
        hg commit -q -m "After update command, date `date "+%Y-%m-%d"`" -u `whoami`
        if [ $? -ne 0 ]; then
            echo "ERROR: An error occurred while commiting changes to HG repository. Abort."
            exit 1
        fi
    fi
'
}

command_Upgrade()
{
    echo ""
    echo "ERROR: upgrade option not yet implemented"
    echo ""
    exit 1

    CURRVER=`getCurrentVersion`
    UPDTVER=`getRemoteVersion update`
	if [[ -n "$CURRVER" ]] && [ "$CURRVER" == "$UPDTVER" ]; then
			echo ""
			echo ""
			echo "ERROR: Last available version $CURRVER already installed."
		exit 1
	fi

	doCurl "download" "${LIBS_FOLDER}_upgrade" "${UPDTVER}"

#	DISTFILE=`ls libs/${PRODUCTNAME}*.zip 2>/dev/null`
#	if [[ -n "$DISTFILE" ]] && [[ -f "$DISTFILE" ]]; then
#		ROOT_FOLDER=`basename ${DISTFILE##*/} .zip`
#		if [ -d $TMP_FOLDER/$ROOT_FOLDER ]; then
#			rm -rf $TMP_FOLDER/$ROOT_FOLDER
#		fi
#		unzip -q -d $TMP_FOLDER $DISTFILE
#		for F in `ls -d $TMP_FOLDER/$ROOT_FOLDER/*`; do
#			if [ -d `basename $F` ]; then
#				rm -rf `basename $F`
#			fi
#			mv -f $F .
#		done
#		rm -rf $TMP_FOLDER/$ROOT_FOLDER
#
#		# setup.sh Should be smart and detect if executing from an update or first install
#		if [[ -f bin/setup.sh ]]; then
#			chmod a+x bin/setup.sh
#			bin/setup.sh
#		fi
#	fi
}

command_Debug()
{
    echo "LASTEST SNAPSHOT"
	echo "--------------------------------------------"
	getRemoteVersionInfo "latestSnapshot"
    echo ""

	echo "LASTEST RELEASE"
    echo "--------------------------------------------"
    getRemoteVersionInfo "latestRelease"
	echo ""

	echo "SNAPSHOTS"
	echo "--------------------------------------------"
	getRemoteVersionInfo "version"  | grep  "SNAPSHOT"
    echo ""

    echo "ALL RELEASES"
	echo "--------------------------------------------"
	getRemoteVersionInfo "version" | grep -v "SNAPSHOT"
	echo ""
}

usage() {
	echo ""
	echo "Usage: `basename "$0"` {info|setup|self-update|download|install|update|upgrade|debug} {PARAMETERS}"
	echo ""
	exit 1
}

error() {
	echo ""
	echo "Error: $1"
	echo ""
	exit 1
}


if [ $# -eq 0 ]; then
	usage
fi

#for arg in "$@"
#do
#   case $arg in
    case $1 in
	    info)
			EXEC_COMMAND=command_Info
			;;
	    setup)
			EXEC_COMMAND=command_Setup
	       	;;
        "self-update")
			EXEC_COMMAND=command_SelfUpdate
			;;
        download)
			EXEC_COMMAND=command_Download
			;;
	    install)
			EXEC_COMMAND=command_Install
			;;
	    update)
			EXEC_COMMAND=command_Update
	       	;;
	    upgrade)
			EXEC_COMMAND=command_Upgrade
	       	;;
	    debug)
			EXEC_COMMAND=command_Debug
	       	;;
	    *)
			usage
			exit 1
			;;
	esac
#done

if [[ "$EXEC_COMMAND" == "command_SelfUpdate" ]]; then
    ${EXEC_COMMAND}
    exit 0;
fi

check_Prerequisites

if [[ -f $INSTALLRC_FILE ]]; then
    source ${INSTALLRC_FILE}
	if [[ "$EXEC_COMMAND" == "command_Setup" ]]; then
    	rm -f $INSTALLRC_FILE
	fi
fi

if [ ! -f "$INSTALLRC_FILE" ] || [ 1 -eq 0 ]; then
	until [[ $CONFIGOK == "y" ]]; do
		echo ""
		echo ""
	    echo "================================================================"
	    echo "CONFIGURATION"
	    echo "================================================================"
		echo ""
	    read -r -p "Nexus Base URL [$NEXUS_BASE_URL]: " INP
	    [ -n "$INP" ] && NEXUS_BASE_URL="$INP"

        # We need this value to get all the remote versions available
        NEXUS_SEARCH_URL="${NEXUS_BASE_URL}service/rest/v1/search/assets/download"
        NEXUS_REPO_URL="${NEXUS_BASE_URL}content/repositories/"
        NEXUS_LUCENE_URL="${NEXUS_BASE_URL}service/local/lucene/search"

	    read -r -p "Nexus User [$NEXUSUSER]: " INP
	    [ -n "$INP" ] && NEXUSUSER="$INP"

	    read -r -s -p "Nexus User Password []: " INP
	    [ -n "$INP" ] && NEXUSPASS="$INP"
        echo ""

	    INP=""
	    until [[ `arrayContainsElement "$INP" "${ARR_PRODUCTS[@]}"` == "y" ]]; do
	    	read -r -p "Product [`printf "%s|" "${ARR_PRODUCTS[@]}"`]: " INP
	    done
	    [ -n "$INP" ] && NEXUSPRODUCT="$INP"

	    # 2016-09-06: No me gusta nada hacer esto aqu�, pero como hay otras dependencias ...
	    case $NEXUSPRODUCT in
		    studio)
		        PRODUCTNAME=axional.studio.startup
                if [[ `inputConfirmation "Do you want to download DB exports for standard dictionaries? [y/n] "` == "Yes" ]]; then
                    NEXUSPRODUCT="studio-w-exports"
                fi
				;;
            microservices)
		        PRODUCTNAME=axional.server.microservices
				;;
			docs)
		        PRODUCTNAME=axional.docs
				;;
		    *)
				PRODUCTNAME=$NEXUSPRODUCT
				;;
		esac

        if [[ `inputConfirmation "Do you want to download from the SNAPSHOT (development) repository? [y/n] "` == "Yes" ]]; then
#TODO: 0.0.+ versions are snapshots!!!!
#TODO: 0.0.+ versions are snapshots!!!!
#TODO: 0.0.+ versions are snapshots!!!!
#TODO: 0.0.+ versions are snapshots!!!!
            PRODUCTVERS="0.0.+"
        else
            echo ""
            echo " Available versions"
            echo "--------------------------------------------"
            getRemoteVersionInfo "version" | grep -v "SNAPSHOT"
            echo ""
            read -r -p "Which version do you want to use? " INP
            [ -n "$INP" ] && PRODUCTVERS="$INP"

            # if no "patch" version is specified (eg: 2017.1), then ".+" is added
            # to download any patch version
            DOT_OCURRENCES=$(grep -o "\." <<< "$PRODUCTVERS" | wc -l)
            if   [[ $DOT_OCURRENCES -eq 1 ]] ;
            then
               PRODUCTVERS="$PRODUCTVERS.+"

            fi
        fi

	    showConfigParams

	    if [[ `inputConfirmation` == "Yes" ]]; then
	    	echo NEXUS_BASE_URL="${NEXUS_BASE_URL}"  > $INSTALLRC_FILE
	    	echo NEXUSUSER="${NEXUSUSER}"           >> $INSTALLRC_FILE
	    	echo NEXUSPASS="${NEXUSPASS}"           >> $INSTALLRC_FILE
	    	echo NEXUSPRODUCT="${NEXUSPRODUCT}"     >> $INSTALLRC_FILE
	    	echo PRODUCTNAME="${PRODUCTNAME}"       >> $INSTALLRC_FILE
	    	echo PRODUCTVERS="${PRODUCTVERS}"       >> $INSTALLRC_FILE

	    	CONFIGOK="y"
	    fi
	done
fi

NEXUS_SEARCH_URL="${NEXUS_BASE_URL}service/rest/v1/search/assets/download"
NEXUS_REPO_URL="${NEXUS_BASE_URL}content/repositories/"

echo ""
echo "PARAMETERS"
echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo INSTALLRC_FILE=$INSTALLRC_FILE
echo NEXUS_SEARCH_URL=$NEXUS_SEARCH_URL
echo NEXUS_REPO_URL=$NEXUS_REPO_URL [$NEXUS_LUCENE_TYPE]
echo NEXUSUSER=$NEXUSUSER
echo NEXUSPRODUCT=$NEXUSPRODUCT
echo PRODUCTNAME=$PRODUCTNAME
echo PRODUCTVERS=$PRODUCTVERS
echo ""

LIBS_FOLDER="libs"

if [[ "$EXEC_COMMAND" == "command_Install" ]]; then
    ${EXEC_COMMAND} "${PRODUCTNAME}"
elif [[ "$EXEC_COMMAND" == "command_Update" ]]; then
    ${EXEC_COMMAND} "${PRODUCTNAME}"
elif [[ "$EXEC_COMMAND" == "command_Download" ]]; then
    ${EXEC_COMMAND} "$2" "${PRODUCTNAME}"
else
    ${EXEC_COMMAND}
fi

exit 0;