
=================================================================
 AXIONAL INSTALLER 
 (STORED IN A GIT REPOSITORY AT BITBUCKET.ORG)
=================================================================

DOWNLOAD/UPDATE INSTALLER FILES (DEV VERSION):

	(using curl)
        $ curl -k -s "https://bitbucket.org/deister/axional-install/get/master.tar.gz" | tar -x -z --strip-components=1 --exclude ".git*"

    (using wget)
        $ wget -q -O - "https://bitbucket.org/deister/axional-install/get/master.tar.gz" | tar -x -z --strip-components=1 --exclude ".git*"


DOWNLOAD/UPDATE INSTALLER FILES (PRE-2018 VERSIONS):

	(using curl)
        $ curl -k -s "https://bitbucket.org/deister/axional-install/get/until-2018.tar.gz" | tar -x -z --strip-components=1 --exclude ".hg*"

    (using wget)
        $ wget -q -O - "https://bitbucket.org/deister/axional-install/get/until-2018.tar.gz" | tar -x -z --strip-components=1 --exclude ".hg*"
